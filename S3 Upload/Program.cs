﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S3_Upload
{
    class Program
    {
        private const string bucketName = "nine9database";
        private const string accessKey = "AKIAY4Y7JABAH5IEDR4E";
        private const string secretKey = "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";
        private const string folder = @"E:\Backup\onesourcetalent.com";
        //private const string folder = @"C:\Users\jason.mortiere\Desktop\TestThis";
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast1;
        private static IAmazonS3 client;

        static void Main(string[] args)
        {
            //Set up creds
            BasicAWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
            client = new AmazonS3Client(awsCredentials, bucketRegion);

            //Get objects already in the bucket
            List<string> backupList = new List<string>();
            ListingObjectsAsync(backupList).Wait();
            List<string> deleteList = new List<string>(backupList);

            //Look at files in backkup folder and upload to S3 if they are not already uploaded
            string[] filePaths = Directory.GetFiles(folder);
            foreach (string filePath in filePaths)
            {
                string fileName = Path.GetFileName(filePath);

                //Do not delete what is currently in the folder
                deleteList.Remove(fileName);

                //Upload if not already uploaded
                if (!backupList.Contains(fileName))
                {
                    try
                    {
                        Console.WriteLine("Uploading file " + fileName);
                        UploadFileAsync(filePath).Wait();
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine("Error uploading because " + ex.Message);
                    }
                }
            };
            //Delete everything in the bucket that is not in the folder
            foreach (string fileToDelete in deleteList)
            {
                Console.WriteLine("Deleting file " + fileToDelete);
                client.DeleteObject(bucketName, fileToDelete);
            }

            //Get remaining list of files to write to table
            List<string> backupList1 = new List<string>();
            ListingObjectsAsync(backupList1).Wait();

            //Delete all existing records
            devEntities entities = new devEntities();
            List<tblITDashboard> list = entities.tblITDashboards.Where(x => x.TypeId == 1).ToList();
            if (list.Count > 0)
            {
                foreach (tblITDashboard dashboard in list)
                {
                    entities.Entry(dashboard).State = System.Data.Entity.EntityState.Deleted;
                }
                entities.SaveChanges();
            }

            //Insert list into table
            devEntities entities1 = new devEntities();
            if (backupList1.Count > 0)
            {
                foreach (string fileName in backupList1)
                {
                    tblITDashboard dashboard = new tblITDashboard() { TypeId = 1, Value = fileName, DateCreated = DateTime.Now };
                    entities1.tblITDashboards.Add(dashboard);
                }
                entities1.SaveChanges();
            }
            Console.WriteLine("Done");
        }
        private static async Task UploadFileAsync(string filePath)
        {
            var fileTransferUtility = new TransferUtility(client);
            var fileTransferUtilityRequest = new TransferUtilityUploadRequest
            {
                BucketName = bucketName,
                FilePath = filePath,
                StorageClass = S3StorageClass.StandardInfrequentAccess,
                PartSize = 50000000
            };
            await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);
        }

        private static async Task ListingObjectsAsync(List<string> backupList)
        {
            ListObjectsV2Request request = new ListObjectsV2Request
            {
                BucketName = bucketName,
                MaxKeys = 10
            };
            ListObjectsV2Response response;
            do
            {
                response = await client.ListObjectsV2Async(request);
                foreach (S3Object entry in response.S3Objects)
                {
                    backupList.Add(entry.Key);
                }
                request.ContinuationToken = response.NextContinuationToken;
            } while (response.IsTruncated);
        }
    }
}
