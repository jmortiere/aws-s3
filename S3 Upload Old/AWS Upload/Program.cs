﻿using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AWS_Upload
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"E:\mssql\databases-backup-cleanup\onesourcetalent.com";
            string bucket = "nine9database";

            string[] listFiles = Directory.GetFiles(path);

            var sort = from fn in listFiles
                       orderby new FileInfo(fn).Length
                       select fn;

            foreach (string fullFilePath in sort)
            {
                string fileName = Path.GetFileName(fullFilePath);

                //Check to see if file exists in S3
                S3FileInfo s = new S3FileInfo(new AmazonS3Client(Amazon.RegionEndpoint.USEast1), bucket, fileName);
                bool alreadyUploaded = s.Exists;

                //Already pushed this file up, continue
                if (alreadyUploaded)
                {
                    continue;
                }

                try
                {
                    TransferUtilityUploadRequest fileTransferUtilityRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = bucket,
                        FilePath = fullFilePath,
                        StorageClass = S3StorageClass.Standard,
                        PartSize = 100000000, // 100 MB
                        Key = fileName,
                        CannedACL = S3CannedACL.PublicRead
                    };
                    TransferUtility fileTransferUtility = new TransferUtility(new AmazonS3Client(Amazon.RegionEndpoint.USEast1));
                    fileTransferUtility.Upload(fileTransferUtilityRequest);
                }
                catch
                {
                }
            }
        }
    }
}
